package com.leeven.grpcserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpcServerSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrpcServerSpringbootApplication.class, args);
    }

}
