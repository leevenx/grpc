package com.leeven.grpcserver.grpcimpl;

import com.leeven.grpc.GreeterGrpc;
import com.leeven.grpc.HelloReply;
import com.leeven.grpc.HelloRequest;
import io.grpc.stub.StreamObserver;
import net.devh.springboot.autoconfigure.grpc.server.GrpcService;

@GrpcService(GreeterGrpc.class)
public class GreeterService extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        HelloReply.Builder builder = HelloReply.newBuilder();
        // ...业务逻辑。。
        builder.setMessage("name:" + request.getName() + ",sex:" + request.getSex());
        HelloReply response = builder.build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
