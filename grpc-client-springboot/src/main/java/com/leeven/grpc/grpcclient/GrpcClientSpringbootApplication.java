package com.leeven.grpc.grpcclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpcClientSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrpcClientSpringbootApplication.class, args);
    }

}
