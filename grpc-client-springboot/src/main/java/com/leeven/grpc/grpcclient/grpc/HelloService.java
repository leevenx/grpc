package com.leeven.grpc.grpcclient.grpc;

import com.leeven.grpc.GreeterGrpc;
import com.leeven.grpc.HelloReply;
import com.leeven.grpc.HelloRequest;
import io.grpc.Channel;
import net.devh.springboot.autoconfigure.grpc.client.GrpcClient;
import org.springframework.stereotype.Service;

@Service
public class HelloService {
    @GrpcClient("grpc-server")
    private Channel serverChannel;

    public String sayHello(String name, String sex) {
        GreeterGrpc.GreeterBlockingStub stub = GreeterGrpc.newBlockingStub(serverChannel);
        HelloReply response = stub.sayHello(HelloRequest.newBuilder().setName(name).setSex(sex).build());
        return response.getMessage();
    }

}
