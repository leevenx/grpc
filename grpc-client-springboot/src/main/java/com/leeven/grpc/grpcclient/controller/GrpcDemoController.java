package com.leeven.grpc.grpcclient.controller;

import com.leeven.grpc.grpcclient.grpc.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GrpcDemoController {
    @Autowired
    private HelloService helloService;

    @RequestMapping("/")
    public String hello(String name, String sex) {
        return helloService.sayHello(name, sex);
    }
}
